# Editeur d'Attaque

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Interface des Attaques de RubyHost
Le bouton `Attaques` accessible sur l'interface d'accueil de RubyHost permet d'accéder à une fenêtre permettant de modifier les données des attaques que peuvent apprendre les Pokémon. Le comportement de l'attaque dépendra de méthodes générales (`:s_basic` pour Charge par exemple) et de méthodes spécifiques programmées dans les scripts.

![Interface de l'éditeur d'attaque|center](img/ruby_host/Rubyhost_skillmain.png "Interface de l'éditeur d'attaque")

## Créer une nouvelle attaque
Pour créer une nouvelle attaque :

1. Assurez vous d'avoir correctement ajouté les entrées de texte qui lui correspondent. (Nom + Description)
2. Cliquez sur `Ajouter une attaque`.
3. Vérifiez que les textes `Nom :` et `Description :` correspondent.
4. Paramétrez les différentes données de l'attaque (Puissance, Précision, PP, etc.).
5. Configurez les caractéristiques de l'attaque (au besoin observez les données des attaques officielles similaires à celle que vous voulez créer).
6. S'il s'agit d'une nouvelle CS, sélectionnez l'ID correspondant à l'événement commun qui sera activé sur la map dans `ID évent commun (MAP) :`.
7. Sélectionnez la `Méthode invoquée en combat :`.
    - `:s_basic` pour Charge
    - `:s_stat` pour Rugissement
    - `:s_status` pour Feu Follet
    - `:s_multi_hit` pour Balle Graine
    - `:s_2hits` pour Coup Double
    - `:s_ohko` pour Abîme
    - `:s_2turns` pour Vol
    - `:s_self_stat` pour Close Combat
    - `:s_self_statut` pour une attaque qui auto-inflige un statut au lanceur.
