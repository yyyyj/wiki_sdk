require 'json'

# Wiki Generator relying on a JSON configuration looking like this :
#   {
#     "generic_head": "filename of the file replacing the <!-- {HEAD} --> section by default",
#     "generic_header": "filename of the file replacing the <!-- {HEADER} --> section by default",
#     "generic_footer": "filename of the file replacing the <!-- {FOOTER} --> section by default",
#     "wiki_sections": [
#       {
#         "dir": "name of the section folder, will search .md in input/#{lang}/#{dir}",
#         "langs": ["en", "fr"],
#         "articles": ["name_of_md1_without_dot_md", "name_of_md2_without_dot_md"],
#         "head": "optional filename replacing generic_head",
#         "header": "optional filename replacing generic_header",
#         "footer": "optional filename replacing generic_footer",
#         "article_header": "Name of the class (in WikiGenerator) used to generate the <!-- {ARTICLE_HEADER} --> section",
#         "article_footer": "Same but for <!-- {ARTICLE_FOOTER} -->"
#       }
#     ]
#   }
class WikiGenerator
  # Constant containing all the lang translations
  LANG_TRANSLATIONS = {
    'fr' => 'Français',
    'en' => 'English',
    'de' => 'Deutsch',
    'it' => 'Italiano',
    'es' => 'Español',
    'ja' => '日本語',
    'ko' => '한국어'
  }
  # Create a new WikiGenerator
  # @param conf_file [String] name of the file containing the wiki configuration
  def initialize(conf_file)
    puts 'Load conf'
    @wiki_sections = JSON.load(File.read(File.join('input', conf_file)))
    # Cache for html document that were loaded (head, header, footer)
    # @type [Hash{String => String}]
    @html_cache = {}
    @generic_head = @wiki_sections['generic_head']
    @generic_header = @wiki_sections['generic_header']
    @generic_footer = @wiki_sections['generic_footer']
    @render_json = ENV['JSON'] == 'true'
    create_renderer
  end

  # Start the processing of the Wiki
  def process
    Dir.mkdir('output') unless Dir.exist?('output')
    @wiki_sections['wiki_sections'].each do |section|
      next unless ARGV.empty? || ARGV.include?(section['dir'])
      section['langs'].each do |lang|
        # Generate the output dir
        lang_dir = File.join('output', lang)
        Dir.mkdir(lang_dir) unless Dir.exist?(lang_dir)
        output_dir = File.join(lang_dir, dir = section['dir'])
        Dir.mkdir(output_dir) unless Dir.exist?(output_dir)
        # Render the wiki section
        @titles = []
        @articles = section['articles'].collect { |md_filename| render("input/#{lang}/#{dir}/#{md_filename}.md", lang) }
        # Generate the actual output
        load_section_html_parts(section, lang)
        generate_output(output_dir, section['articles'])
      end
    end
  end

  private

  # Load the section html parts
  # @param section [Hash]
  # @param lang [String]
  def load_section_html_parts(section, lang)
    @head = load_html_part(File.join(section['head'] || @generic_head))
    @header = load_html_part(File.join(lang, section['header'] || @generic_header))
    @footer = load_html_part(File.join(lang, section['footer'] || @generic_footer))
    header_class = self.class.const_get(section['article_header'] || :GenericArticleHeader, false)
    # @type [GenericArticleHeader]
    @article_header = header_class.new(@titles, section['dir'], section['articles'], section['langs'], lang)
    footer_class = self.class.const_get(section['article_footer'] || :GenericArticleFooter, false)
    # @type [GenericArticleFooter]
    @article_footer = footer_class.new(@titles, section['dir'], section['articles'], section['langs'], lang)
  end

  # Load a html part document
  # @param filename [String]
  # @return [String] the document contents
  def load_html_part(filename)
    @html_cache[filename] ||= File.read("input/html_parts/#{filename}.html")
  end

  # Render a Wiki Page and store its title in @titles array
  # @param filename [String] name of the file to render
  # @param lang [String] language used for render
  # @return [String] rendered file
  def render(filename, lang)
    puts "Rendering #{filename}"
    @renderer.init(lang)
    filename = "input/wip_#{lang}.md" unless File.exist?(filename)
    rendered_file = @markdown.render(File.read(filename))
    @titles << @renderer.title
    return rendered_file
  end

  # Generate the output dir for this section
  # @param output_dir [String] output folder for articles
  # @param article_filenames [Array<String>] filename of each articles
  def generate_output(output_dir, article_filenames)
    puts "Generating output directory : #{output_dir}"
    ext = @render_json ? '.json' : '.html'
    article_filenames.each_with_index do |filename, index|
      File.write(
        File.join(output_dir, filename) + ext,
        format_page(@articles[index], index)
      )
    end
  end

  # Format an article page
  # @param page [String] html page to format
  # @param index [Integer] index of the html page (used for article header/footer)
  def format_page(page, index)
    return page if @render_json
    page.sub!('<!-- {HEAD} -->', @head)
    page.sub!('<!-- {HEADER} -->', @header)
    page.sub!('<!-- {FOOTER} -->', @footer)
    page.sub!('<!-- {ARTICLE_HEADER} -->', @article_header.process(index))
    page.sub!('<!-- {ARTICLE_FOOTER} -->', @article_footer.process(index))
    fix_links(page)
    return page
  end

  # Fix the links inside the page
  #
  # This will replace all the href="css/***" by href="#{ENV['WIKI_CSS_PATH']}/***",
  # href="lang/***" by href="#{ENV['WIKI_PATH']}/lang/***" and
  # src="img/***" by src="#{ENV['WIKI_IMG_PATH']}/***"
  def fix_links(page)
    page.gsub!(%r{href="css/}, "href=\"#{ENV['WIKI_CSS_PATH'] || File.expand_path('input/css')}/")
    page.gsub!(%r`href="([a-z]{2})/`, "href=\"#{ENV['WIKI_PATH'] || File.expand_path('output')}/\\1/")
    page.gsub!(/href="([^"]+)\.md"/, 'href="\1.html"')
    page.gsub!(%r{src="img/}, "src=\"#{ENV['WIKI_IMG_PATH'] || File.expand_path('input/img')}/")
  end

  # Create the Markdown renderer
  def create_renderer
    puts 'Creating renderers'
    @renderer = (@render_json ? JsonRenderer : CustomRenderer).new(
      filter_html:     false,
      hard_wrap:       false # ,
      # link_attributes: { target: '_blank' }
    )
    @markdown = Redcarpet::Markdown.new(
      @renderer,
      fenced_code_blocks: true,
      no_intra_emphasis: true,
      strikethrough: true,
      footnotes: true,
      tables: true,
      lax_spacing: true
    )
  end
end
Dir['lib/wiki_generator/*.rb'].sort.each { |filename| require "./#{filename}" }
